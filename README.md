# Project Summary

For more information, please contact: eelco.klaver@credenco.com

## Summary
G3C (Good Conduct Certificate Credential) provides a solution for issuing, verifying, processing and archiving digital Certificates of Good Conducts.

## The problem
More than 1 million organizations in the Netherlands process paper Certificate of Conducts, about 340 are large organizations. It is mandatory for them to request, verify, process and archive these certificates, in most cases annually and for each new or temporary employee.

Customer pains
- It is a lot of work to copy and re-enter the data manually from the (paper) document into their back-
office and archive systems. This is a manual, **labor-intensive** and **error-prone** process.
- The current paper document is **easy to tamper** with. Even more with scanned copies.
- There is no way to **revoke** the documents. If the facts on which the certificate was granted change (e.g. when the employee gets a criminal record), the paper is still valid and in circulation.

What the customer would like to see:
- An automated process to capture and periodically verify & authenticate the data.
- Traditional document processing systems can only do the capture part and are error-prone due to errors in Optical Character Recognition (OCR). Moreover, these documents are far from tamperproof, and the tampering is often not easy to spot. If your livelihood depends on it, fraud is very tempting.

The problem is so urgent that several large organizations already requested to participate in the pilot phase of our solution.

## Solution
We provide a solution for issuing, verifying, processing and archiving digital Certificates of Good Conducts with the following characteristics: 

**Privacy preserving, control over your own data, GDPR friendly**
- This will improve the privacy of the citizens controlling which data they share and with whom
- Organizations do not necessarily have to store the person’s data themselves. Registration of the receipt and verification (including the unique reference to the VC) should be sufficient, according to Justis.

**Providing automatic, Straight Through Processing (STP/RPA) of data**
- This saves organizations significant amounts of work, time and money
- Shorter cycle means the employee/citizen can start quicker

**Any tampering with data is immediately evident**
- This will prevent attempts of fraud and expose bad actors

**Supports intermediate revocation of the CoCG credential**
- This will improve safety and security for the goal and purpose for which the CoCG certificate is being issued

## Ecosystem
The solution consists of the following components

- A verifiable credential schema for Digital Certificates of Good Conduct
- Credenco Platform for issuing and verifying Digital Certificates of Good Conducts
- A wallet for the holder of the certificates with the ability to share the Certificate of Good Conduct with employers and institutions

The solution will integrate with the following existing eSSIF components
- **Verifier Universal Interface** - Parts of Verifiable Universal Interface (Gataca) are used to ensure maximum interoperability, given we want to seek maximum integration possibilities and a reference open-source implementation of Justis issued credentials.
- **Presentation Exchange Credential Query** - The Presentation Exchange (Sphereon) will be used to negotiate the required verification of the different VCs issued by Justis.
- **SSI eIDAS bridge** - the eIDAS bridge (Validated ID/Off-Blocks) will be used to sign VCs issued by Justis, using X509 certificates.
